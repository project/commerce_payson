-- SUMMARY --

The Commerce Payson module integrates a payment method with Swedish Payson 
for the Drupal Commerce payment and checkout system.

For a full description of the module, visit the project page:
  https://drupal.org/sandbox/martinsrensson/1561188
  
-- REQUIREMENTS --

Requires Commerce, Commerce UI, Commerce Payment and Commerce Order

-- GOALS AND LIMITATIONS -- 
  
For now this module only offers integration with the Payson Agent Integration.
In future releases it might be possible to offer API integration as well.

-- CONTACT --

Current maintainers:

Martin sörensson - http://drupal.org/user/806972
